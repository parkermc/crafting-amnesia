package com.parkermc.craftingamnesia.jei;

import com.parkermc.craftingamnesia.recipes.EditableRecipe;

import mezz.jei.api.ingredients.IIngredients;
import mezz.jei.api.recipe.BlankRecipeWrapper;
import mezz.jei.api.recipe.wrapper.ICraftingRecipeWrapper;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.ShapelessRecipes;

public class EditableShapelessRecipeWrapper extends BlankRecipeWrapper implements ICraftingRecipeWrapper {

	private final EditableRecipe recipe;

	public EditableShapelessRecipeWrapper(EditableRecipe recipe) {
		this.recipe = recipe;
		for (Object input : ((ShapelessRecipes)this.recipe.recipe).recipeItems) {
			if (input instanceof ItemStack) {
				ItemStack itemStack = (ItemStack) input;
				if (itemStack.getCount() != 1) {
					itemStack.setCount(1);
				}
			}
		}
	}

	@Override
	public void getIngredients(IIngredients ingredients) {
		ItemStack recipeOutput = recipe.getRecipeOutput();

		try {
			ingredients.setInputs(ItemStack.class, ((ShapelessRecipes)this.recipe.recipe).recipeItems);
			ingredients.setOutput(ItemStack.class, recipeOutput);
		} catch (RuntimeException e) {
		}
	}
}
