package com.parkermc.craftingamnesia.recipes;

import com.parkermc.craftingamnesia.recipes.RecipeManager;

import net.minecraft.inventory.InventoryCrafting;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.IRecipe;
import net.minecraft.util.NonNullList;
import net.minecraft.world.World;

public class EditableRecipe implements IRecipe{
	public final IRecipe recipe;
	public final int id;

	public EditableRecipe(IRecipe recipe, int id) {
		this.recipe = recipe;
		this.id = id;
	}
	
	public ItemStack getRealOutput() {
		return recipe.getRecipeOutput();
	}
	
	@Override
	public ItemStack getCraftingResult(InventoryCrafting inv) {
		return this.getRecipeOutput();
	}
	
	@Override
	public ItemStack getRecipeOutput() {
		if(RecipeManager.instance != null) {
			ItemStack output = RecipeManager.instance.getOutput(this);
			if(output != null) {
				return output.copy();
			}
		}
		return this.recipe.getRecipeOutput().copy();
	}
	
	@Override
	public boolean matches(InventoryCrafting inv, World worldIn) {
		return recipe.matches(inv, worldIn);
	}
	
	@Override
	public NonNullList<ItemStack> getRemainingItems(InventoryCrafting inv){
        return recipe.getRemainingItems(inv);
    }
	
	@Override
	public int getRecipeSize() {
		return recipe.getRecipeSize();
	}
}
