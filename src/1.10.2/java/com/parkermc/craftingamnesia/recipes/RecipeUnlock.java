package com.parkermc.craftingamnesia.recipes;

import com.parkermc.craftingamnesia.ModItems;
import com.parkermc.craftingamnesia.items.ItemLock;

import net.minecraft.inventory.InventoryCrafting;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.IRecipe;
import net.minecraft.world.World;

public class RecipeUnlock implements IRecipe {
	
	@Override
	public boolean matches(InventoryCrafting inv, World worldIn) {
		
		boolean lock = false;
		boolean otherItem = false;
		ItemStack other = null;
		int size = inv.getWidth()*inv.getHeight();
		for(int i=0; i < size; i++) {
			if(inv.getStackInSlot(i) != null) {
				if(ItemLock.isLock(inv.getStackInSlot(i).getItem())) {
					if(lock) {
						return false;
					}else {
						lock = true;
					}
				}else {
					if(otherItem) {
						return false;
					}else {
						otherItem = true;
						other = inv.getStackInSlot(i);
					}
				}
			}
		}
		if(worldIn != null && !worldIn.isRemote) {
			return lock && otherItem && RecipeManager.instance.canUnlock(other);
		}else {
			return lock && otherItem;
		}
	}

	@Override
	public ItemStack getCraftingResult(InventoryCrafting inv) {
		int size = inv.getWidth()*inv.getHeight();
		for(int i=0; i < size; i++) {
			if(inv.getStackInSlot(i) != null&&!ItemLock.isLock(inv.getStackInSlot(i).getItem())) {
				ItemStack stack = inv.getStackInSlot(i).copy();
				stack.stackSize = 1;
				return stack;
			}
		}
		return null;
	}

	@Override
	public ItemStack getRecipeOutput() {
		return new ItemStack(ModItems.lock, 1);
	}

	@Override
    public ItemStack[] getRemainingItems(InventoryCrafting inv){
		return new ItemStack[inv.getSizeInventory()];
    }
	
	public static String getItemString(ItemStack stack) {
		if(stack != null && stack.getItem() != null) {
			return stack.getItem().getRegistryName().toString() + ":" + stack.getMetadata();
		}
		return "";		
	}

	@Override
	public int getRecipeSize() {
		return 2;
	}
}
