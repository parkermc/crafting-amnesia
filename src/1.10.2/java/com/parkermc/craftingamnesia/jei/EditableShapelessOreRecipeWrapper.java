package com.parkermc.craftingamnesia.jei;

import java.util.Collections;
import java.util.List;

import com.parkermc.craftingamnesia.recipes.EditableRecipe;

import mezz.jei.api.IJeiHelpers;
import mezz.jei.api.ingredients.IIngredients;
import mezz.jei.api.recipe.IStackHelper;
import net.minecraft.item.ItemStack;
import net.minecraftforge.oredict.ShapelessOreRecipe;

public class EditableShapelessOreRecipeWrapper extends AbstractShapelessRecipeWrapper {
	private final IJeiHelpers jeiHelpers;
	private final EditableRecipe recipe;

	public EditableShapelessOreRecipeWrapper(IJeiHelpers jeiHelpers, EditableRecipe recipe) {
		super(jeiHelpers.getGuiHelper());
		this.jeiHelpers = jeiHelpers;
		this.recipe = recipe;
		for (Object input : ((ShapelessOreRecipe)this.recipe.recipe).getInput()) {
			if (input instanceof ItemStack) {
				ItemStack itemStack = (ItemStack) input;
				if (itemStack.stackSize != 1) {
					itemStack.stackSize = 1;
				}
			}
		}
	}

	@Override
	public void getIngredients(IIngredients ingredients) {
		IStackHelper stackHelper = jeiHelpers.getStackHelper();
		ItemStack recipeOutput = recipe.getRecipeOutput();

		try {
			List<List<ItemStack>> inputs = stackHelper.expandRecipeItemStackInputs(((ShapelessOreRecipe)this.recipe.recipe).getInput());
			ingredients.setInputLists(ItemStack.class, inputs);

			if (recipeOutput != null) {
				ingredients.setOutput(ItemStack.class, recipeOutput);
			}
		} catch (RuntimeException e) {
		}
	}

	@SuppressWarnings("rawtypes")
	@Override
	public List getInputs() {
		return ((ShapelessOreRecipe)this.recipe.recipe).getInput();
	}

	@Override
	public List<ItemStack> getOutputs() {
		return Collections.singletonList(recipe.getRecipeOutput());
	}
}
