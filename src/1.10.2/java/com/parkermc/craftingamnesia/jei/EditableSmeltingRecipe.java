package com.parkermc.craftingamnesia.jei;


import java.awt.Color;
import java.util.Collections;
import java.util.IllegalFormatException;
import java.util.List;

import mezz.jei.api.ingredients.IIngredients;
import mezz.jei.api.recipe.BlankRecipeWrapper;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.FurnaceRecipes;
import net.minecraft.util.text.translation.I18n;

@SuppressWarnings("deprecation")
public class EditableSmeltingRecipe extends BlankRecipeWrapper {
	private final List<List<ItemStack>> inputs;
	private final ItemStack output;

	public EditableSmeltingRecipe(List<ItemStack> inputs, ItemStack output) {
		this.inputs = Collections.singletonList(inputs);
		this.output = output;
	}

	@Override
	public void getIngredients(IIngredients ingredients) {
		ingredients.setInputLists(ItemStack.class, inputs);
		ingredients.setOutput(ItemStack.class, output);
	}

	public List<List<ItemStack>> getInputs() {
		return inputs;
	}

	public List<ItemStack> getOutputs() {
		return Collections.singletonList(output);
	}

	@Override
	public void drawInfo(Minecraft minecraft, int recipeWidth, int recipeHeight, int mouseX, int mouseY) {
		FurnaceRecipes furnaceRecipes = FurnaceRecipes.instance();
		float experience;
		try {
			experience = furnaceRecipes.getSmeltingExperience(output);
		} catch (RuntimeException ignored) {
			experience = 0;
		}
		if (experience > 0) {
			String experienceString;
			String s;
			if (I18n.canTranslate("gui.jei.category.smelting.experience")) {
				s = I18n.translateToLocal("gui.jei.category.smelting.experience");
			} else {
				s = I18n.translateToFallback("gui.jei.category.smelting.experience");
			}
			try {
				experienceString = String.format(s, experience);
			} catch (IllegalFormatException e) {
				experienceString = "Format error: " + s;
			}
			FontRenderer fontRendererObj = minecraft.fontRendererObj;
			int stringWidth = fontRendererObj.getStringWidth(experienceString);
			fontRendererObj.drawString(experienceString, recipeWidth - stringWidth, 0, Color.gray.getRGB());
		}
	}
}
