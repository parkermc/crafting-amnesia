package com.parkermc.craftingamnesia.jei;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.parkermc.craftingamnesia.recipes.EditableRecipe;
import com.parkermc.craftingamnesia.recipes.ItemCompare;

import mezz.jei.api.IJeiHelpers;
import mezz.jei.api.recipe.IFocus;
import mezz.jei.api.recipe.IRecipeCategory;
import mezz.jei.api.recipe.IRecipeRegistryPlugin;
import mezz.jei.api.recipe.IRecipeWrapper;
import mezz.jei.api.recipe.IStackHelper;
import mezz.jei.api.recipe.VanillaRecipeCategoryUid;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.CraftingManager;
import net.minecraft.item.crafting.FurnaceRecipes;
import net.minecraft.item.crafting.IRecipe;
import net.minecraft.item.crafting.ShapedRecipes;
import net.minecraft.item.crafting.ShapelessRecipes;
import net.minecraftforge.oredict.ShapedOreRecipe;
import net.minecraftforge.oredict.ShapelessOreRecipe;

public class EditableRecipeRegistryPlugin implements IRecipeRegistryPlugin{

	private IJeiHelpers iJeiHelpers;
	private List<IRecipeWrapper> recipes = new ArrayList<IRecipeWrapper>();
	private Map<ItemCompare, List<IRecipeWrapper>> outputMap = new HashMap<ItemCompare, List<IRecipeWrapper>>();
	private Map<ItemCompare, List<IRecipeWrapper>> inputMap = new HashMap<ItemCompare, List<IRecipeWrapper>>();
	private List<IRecipeWrapper> furRecipes = new ArrayList<IRecipeWrapper>();
	private Map<ItemCompare, List<IRecipeWrapper>> outputFurMap = new HashMap<ItemCompare, List<IRecipeWrapper>>();
	private Map<ItemCompare, List<IRecipeWrapper>> inputFurMap = new HashMap<ItemCompare, List<IRecipeWrapper>>();

	public EditableRecipeRegistryPlugin(IJeiHelpers iJeiHelpers){
		this.iJeiHelpers = iJeiHelpers;
		this.Regen();
	}
	
	@Override
	public <V> List<String> getRecipeCategoryUids(IFocus<V> focus) {
		if(focus.getValue() instanceof ItemStack) {
			ItemCompare regItem = new ItemCompare((ItemStack)focus.getValue());
			if(focus.getMode() == IFocus.Mode.INPUT) {
				ArrayList<String> categories = new ArrayList<String>();
				if(this.inputMap.containsKey(regItem)) {
					categories.add(VanillaRecipeCategoryUid.CRAFTING);
				}
				if(this.inputFurMap.containsKey(regItem)) {
					categories.add(VanillaRecipeCategoryUid.SMELTING);
				}
				return categories;
			}else {
				ArrayList<String> categories = new ArrayList<String>();
				if(this.outputMap.containsKey(regItem)) {
					categories.add(VanillaRecipeCategoryUid.CRAFTING);
				}
				if(this.outputFurMap.containsKey(regItem)) {
					categories.add(VanillaRecipeCategoryUid.SMELTING);
				}
				return categories;
			}
		}
		return new ArrayList<String>();
	}

	@SuppressWarnings("unchecked")
	@Override
	public <T extends IRecipeWrapper, V> List<T> getRecipeWrappers(IRecipeCategory<T> recipeCategory, IFocus<V> focus) {
		if(recipeCategory.getUid().equals(VanillaRecipeCategoryUid.CRAFTING)) {
			if(focus.getValue() instanceof ItemStack) {
				ItemStack item = (ItemStack)focus.getValue();
				if(focus.getMode() == IFocus.Mode.INPUT) {
					if(this.inputMap.containsKey(new ItemCompare(item))) {
						return (List<T>) this.inputMap.get(new ItemCompare(item));
					}
				}else {
					if(this.outputMap.containsKey(new ItemCompare(item))) {
						return (List<T>) this.outputMap.get(new ItemCompare(item));
					}
				}
			}
		}else if(recipeCategory.getUid().equals(VanillaRecipeCategoryUid.SMELTING)) {
			if(focus.getValue() instanceof ItemStack) {
				ItemStack item = (ItemStack)focus.getValue();
				if(focus.getMode() == IFocus.Mode.INPUT) {
					if(this.inputFurMap.containsKey(new ItemCompare(item))) {
						return (List<T>) this.inputFurMap.get(new ItemCompare(item));
					}
				}else {
					if(this.outputFurMap.containsKey(new ItemCompare(item))) {
						return (List<T>) this.outputFurMap.get(new ItemCompare(item));
					}
				}
			}
		}
		return new ArrayList<T>();
	}
	
	private List<ItemStack> getItems(List<Object> oa){
		List<ItemStack> out = new ArrayList<ItemStack>();
		for(Object o : oa) {
			if(o instanceof List) {
				for(Object o2 : (List<Object>)oa) {
					if(o2 instanceof ItemStack) {
						out.add((ItemStack)o2);
					}
				}
			}else if(o instanceof ItemStack) {
				out.add((ItemStack)o);
			}
		}
		
		return out;
	}
	
	public void Regen() {
		this.recipes.clear();
		this.outputMap.clear();
		this.inputMap.clear();
		
		Iterator<IRecipe> recipeIterator = CraftingManager.getInstance().getRecipeList().iterator();
		while (recipeIterator.hasNext()) {
			IRecipe recipe = recipeIterator.next();
			if(recipe instanceof EditableRecipe) {
				this.recipes.add(EditableRecipeWrapper.New(this.iJeiHelpers, ((EditableRecipe)recipe)));
				ItemCompare regItem = new ItemCompare(((EditableRecipe)recipe).getRecipeOutput());
				if(this.recipes.get(this.recipes.size()-1) != null) {
					if(!this.outputMap.containsKey(regItem)) {
						this.outputMap.put(regItem, new ArrayList<IRecipeWrapper>());
					}
					this.outputMap.get(regItem).add(this.recipes.get(this.recipes.size()-1));
					List<ItemStack> ingredients = new ArrayList<ItemStack>();
					if(((EditableRecipe)recipe).recipe instanceof ShapelessRecipes) { 
						ingredients.addAll(((ShapelessRecipes)((EditableRecipe)recipe).recipe).recipeItems);
					}else if(((EditableRecipe)recipe).recipe instanceof ShapedRecipes) { 
						ingredients.addAll(Arrays.asList(((ShapedRecipes)((EditableRecipe)recipe).recipe).recipeItems));
					}else if(((EditableRecipe)recipe).recipe instanceof ShapelessOreRecipe) {
						ingredients.addAll(this.getItems(((ShapelessOreRecipe)((EditableRecipe)recipe).recipe).getInput()));
					}else if(((EditableRecipe)recipe).recipe instanceof ShapedOreRecipe) { 
						ingredients.addAll(this.getItems(Arrays.asList(((ShapedOreRecipe)((EditableRecipe)recipe).recipe).getInput())));
					}
					for(ItemStack itemStack : ingredients) {
						if(itemStack != null) {
							regItem = new ItemCompare(itemStack);
							if(!this.inputMap.containsKey(regItem)) {
								this.inputMap.put(regItem, new ArrayList<IRecipeWrapper>());
							}
							this.inputMap.get(regItem).add(this.recipes.get(this.recipes.size()-1));
						}
					}
				}
			}
		}
		
		
		this.furRecipes.clear();
		this.outputFurMap.clear();
		this.inputFurMap.clear();
		
		IStackHelper stackHelper = this.iJeiHelpers.getStackHelper();
	
		for (Map.Entry<ItemStack, ItemStack> entry : FurnaceRecipes.instance().getSmeltingList().entrySet()) {
			ItemStack input = entry.getKey();
			ItemStack output = entry.getValue();

			List<ItemStack> inputs = stackHelper.getSubtypes(input);
			EditableSmeltingRecipe recipe = new EditableSmeltingRecipe(inputs, output);
			this.furRecipes.add(recipe);
			
			ItemCompare regItem = new ItemCompare(output);
			if(this.furRecipes.get(this.furRecipes.size()-1) != null) {
				if(!this.outputFurMap.containsKey(regItem)) {
					this.outputFurMap.put(regItem, new ArrayList<IRecipeWrapper>());
				}
				this.outputFurMap.get(regItem).add(this.furRecipes.get(this.furRecipes.size()-1));
				for(ItemStack itemStack : inputs) {
					regItem = new ItemCompare(itemStack);
					if(!this.inputFurMap.containsKey(regItem)) {
						this.inputFurMap.put(regItem, new ArrayList<IRecipeWrapper>());
					}
					this.inputFurMap.get(regItem).add(this.furRecipes.get(this.furRecipes.size()-1));
				}
			}
		}
	}	
}
