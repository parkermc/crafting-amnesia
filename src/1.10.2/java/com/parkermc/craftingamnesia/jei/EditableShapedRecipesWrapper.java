package com.parkermc.craftingamnesia.jei;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import com.parkermc.craftingamnesia.recipes.EditableRecipe;

import mezz.jei.api.ingredients.IIngredients;
import mezz.jei.api.recipe.BlankRecipeWrapper;
import mezz.jei.api.recipe.wrapper.IShapedCraftingRecipeWrapper;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.ShapedRecipes;

public class EditableShapedRecipesWrapper extends BlankRecipeWrapper implements IShapedCraftingRecipeWrapper {

	private final EditableRecipe recipe;

	public EditableShapedRecipesWrapper(EditableRecipe recipe) {
		this.recipe = recipe;
		for (ItemStack itemStack : ((ShapedRecipes)this.recipe.recipe).recipeItems) {
			if (itemStack != null && itemStack.stackSize != 1) {
				itemStack.stackSize = 1;
			}
		}
	}

	@Override
	public void getIngredients(IIngredients ingredients) {
		List<ItemStack> recipeItems = Arrays.asList(((ShapedRecipes)this.recipe.recipe).recipeItems);
		ItemStack recipeOutput = recipe.getRecipeOutput();
		try {
			ingredients.setInputs(ItemStack.class, recipeItems);
			if (recipeOutput != null) {
				ingredients.setOutput(ItemStack.class, recipeOutput);
			}
		} catch (RuntimeException e) {
		}
	}

	@SuppressWarnings("rawtypes")
	@Override
	public List getInputs() {
		return Arrays.asList(((ShapedRecipes)this.recipe.recipe).recipeItems);
	}

	@Override
	public List<ItemStack> getOutputs() {
		return Collections.singletonList(recipe.getRecipeOutput());
	}

	@Override
	public int getWidth() {
		return ((ShapedRecipes)this.recipe.recipe).recipeWidth;
	}

	@Override
	public int getHeight() {
		return ((ShapedRecipes)this.recipe.recipe).recipeHeight;
	}
}
