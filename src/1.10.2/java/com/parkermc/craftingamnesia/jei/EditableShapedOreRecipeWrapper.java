package com.parkermc.craftingamnesia.jei;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import com.parkermc.craftingamnesia.recipes.EditableRecipe;

import mezz.jei.api.IJeiHelpers;
import mezz.jei.api.ingredients.IIngredients;
import mezz.jei.api.recipe.BlankRecipeWrapper;
import mezz.jei.api.recipe.IStackHelper;
import mezz.jei.api.recipe.wrapper.IShapedCraftingRecipeWrapper;
import net.minecraft.item.ItemStack;
import net.minecraftforge.fml.common.ObfuscationReflectionHelper;
import net.minecraftforge.oredict.ShapedOreRecipe;

public class EditableShapedOreRecipeWrapper extends BlankRecipeWrapper implements IShapedCraftingRecipeWrapper {
	private final IJeiHelpers jeiHelpers;
	private final EditableRecipe recipe;
	private final int width;
	private final int height;

	public EditableShapedOreRecipeWrapper(IJeiHelpers jeiHelpers, EditableRecipe recipe) {
		this.jeiHelpers = jeiHelpers;
		this.recipe = recipe;
		for (Object input : ((ShapedOreRecipe)this.recipe.recipe).getInput()) {
			if (input instanceof ItemStack) {
				ItemStack itemStack = (ItemStack) input;
				if (itemStack.stackSize != 1) {
					itemStack.stackSize = 1;
				}
			}
		}
		this.width = ObfuscationReflectionHelper.getPrivateValue(ShapedOreRecipe.class, ((ShapedOreRecipe)this.recipe.recipe), "width");
		this.height = ObfuscationReflectionHelper.getPrivateValue(ShapedOreRecipe.class, ((ShapedOreRecipe)this.recipe.recipe), "height");
	}

	@Override
	public void getIngredients(IIngredients ingredients) {
		IStackHelper stackHelper = jeiHelpers.getStackHelper();
		ItemStack recipeOutput = recipe.getRecipeOutput();

		try {
			List<List<ItemStack>> inputs = stackHelper.expandRecipeItemStackInputs(Arrays.asList(((ShapedOreRecipe)this.recipe.recipe).getInput()));
			ingredients.setInputLists(ItemStack.class, inputs);
			if (recipeOutput != null) {
				ingredients.setOutput(ItemStack.class, recipeOutput);
			}
		} catch (RuntimeException e) {
		}
	}

	@SuppressWarnings("rawtypes")
	@Override
	public List getInputs() {
		return Arrays.asList(((ShapedOreRecipe)this.recipe.recipe).getInput());
	}

	@Override
	public List<ItemStack> getOutputs() {
		return Collections.singletonList(recipe.getRecipeOutput());
	}

	@Override
	public int getWidth() {
		return width;
	}

	@Override
	public int getHeight() {
		return height;
	}

}
