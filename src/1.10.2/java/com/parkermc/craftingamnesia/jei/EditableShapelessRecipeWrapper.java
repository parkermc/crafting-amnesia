package com.parkermc.craftingamnesia.jei;

import java.util.Collections;
import java.util.List;

import com.parkermc.craftingamnesia.recipes.EditableRecipe;

import mezz.jei.api.IGuiHelper;
import mezz.jei.api.ingredients.IIngredients;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.ShapelessRecipes;

public class EditableShapelessRecipeWrapper extends AbstractShapelessRecipeWrapper {

	private final EditableRecipe recipe;

	public EditableShapelessRecipeWrapper(IGuiHelper guiHelper, EditableRecipe recipe) {
		super(guiHelper);
		this.recipe = recipe;
		for (Object input : ((ShapelessRecipes)this.recipe.recipe).recipeItems) {
			if (input instanceof ItemStack) {
				ItemStack itemStack = (ItemStack) input;
				if (itemStack.stackSize != 1) {
					itemStack.stackSize = 1;
				}
			}
		}
	}

	@Override
	public void getIngredients(IIngredients ingredients) {
		ItemStack recipeOutput = recipe.getRecipeOutput();

		try {
			ingredients.setInputs(ItemStack.class, ((ShapelessRecipes)this.recipe.recipe).recipeItems);
			if (recipeOutput != null) {
				ingredients.setOutput(ItemStack.class, recipeOutput);
			}
		} catch (RuntimeException e) {
		}
	}

	@Override
	public List<ItemStack> getInputs() {
		return ((ShapelessRecipes)this.recipe.recipe).recipeItems;
	}

	@Override
	public List<ItemStack> getOutputs() {
		return Collections.singletonList(recipe.getRecipeOutput());
	}
}
