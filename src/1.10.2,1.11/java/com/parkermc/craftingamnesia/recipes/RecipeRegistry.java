package com.parkermc.craftingamnesia.recipes;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.CraftingManager;
import net.minecraft.item.crafting.FurnaceRecipes;
import net.minecraft.item.crafting.IRecipe;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;

public class RecipeRegistry {
	private Map<Integer,RecipeSave> recipes = new HashMap<Integer,RecipeSave>();
	
	public RecipeRegistry() {
	}
	
	public RecipeRegistry(NBTTagCompound nbt) {
		this.readFromNBT(nbt);
	}
	
	public void add(RecipeSave recipe) {
		this.recipes.put(recipe.getID(), recipe);
	}
	
	public void applyFurnace() {
		List<RecipeSave> furnaces = new ArrayList<RecipeSave>();
		for(RecipeSave recipe : this.recipes.values()) {
			if(recipe.isFurnace()) {
				furnaces.add(recipe);
			}
		}
		
		for(ItemStack input : FurnaceRecipes.instance().getSmeltingList().keySet()) {
			for(RecipeSave recipe : furnaces) {
				if(new ItemCompare(input).equals(new ItemCompare(recipe.getInput()))) {
					FurnaceRecipes.instance().getSmeltingList().replace(input, recipe.getOutput());
				}
			}
		}
	}
	
	public ItemStack getOutput(EditableRecipe recipe) {
		if(this.recipes.containsKey(recipe.id)) {
			return this.recipes.get(recipe.id).getOutput().copy();
		}
		return null;
	}
	
	public List<ItemStack> getOutputs(){
		List<ItemStack> out = new ArrayList<ItemStack>();
		for(RecipeSave recipe : this.recipes.values()) {
			out.add(recipe.getOutput().copy());
		}
		return out;
	}
	
	public List<RecipeSave> getRecipes(){
		return new ArrayList<RecipeSave>(this.recipes.values());
	}
	
	public int size() {
		return this.recipes.size();
	}

	public void readFromNBT(NBTTagCompound nbt) {
		NBTTagList shapedTag = nbt.getTagList("recipes", 10);
		List<RecipeSave> loadedRecipes = new ArrayList<RecipeSave>();
		for(int i=0;i<shapedTag.tagCount();i++) {
			RecipeSave recipe = new RecipeSave(shapedTag.getCompoundTagAt(i));
			if(recipe.isFurnace()) {
				this.recipes.put(recipe.getID(), recipe);
			}else {
				loadedRecipes.add(recipe);
			}
		}
		
		for(IRecipe recipe : CraftingManager.getInstance().getRecipeList()) {
			if(recipe instanceof EditableRecipe) {
				RecipeSave recipeSave = new RecipeSave((EditableRecipe)recipe);
				for(RecipeSave recipe2 : loadedRecipes) {
					if(recipe2.equals(recipeSave)) {
						recipe2.setID(recipeSave.getID());
						recipe2.setRealOutput(((EditableRecipe) recipe).getRealOutput());
						this.recipes.put(recipe2.getID(), recipe2);
						loadedRecipes.remove(recipe2);
						break;
					}
				}
			}
		}
	}

	public NBTTagCompound writeToNBT(NBTTagCompound compound) {
		NBTTagList recipesTag = new NBTTagList();
		for(RecipeSave recipe : this.recipes.values()) {
			recipesTag.appendTag(recipe.writeToNBT(new NBTTagCompound()));
		}
		compound.setTag("recipes", recipesTag);
		return compound;
	}
}
