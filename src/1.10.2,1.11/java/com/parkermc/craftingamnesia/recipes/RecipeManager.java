package com.parkermc.craftingamnesia.recipes;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Random;

import com.parkermc.amnesia.ModTools;
import com.parkermc.craftingamnesia.ModMain;
import com.parkermc.craftingamnesia.network.MessageRecipes;
import com.parkermc.craftingamnesia.proxy.ProxyCommon;

import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.CraftingManager;
import net.minecraft.item.crafting.FurnaceRecipes;
import net.minecraft.item.crafting.IRecipe;
import net.minecraft.nbt.CompressedStreamTools;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.nbt.NBTTagString;
import net.minecraft.world.World;
import net.minecraftforge.fml.common.FMLLog;

public class RecipeManager{
	private static String folder = "data";
	private static String file = "crafting_amnesia.dat";
	private static int lastID = 0;
	
	public static RecipeManager instance;
	
	private List<String> unlockedItems = new ArrayList<String>();
	private World world;
	public RecipeRegistry normalRecipes;
	public RecipeRegistry recipes = new RecipeRegistry();
	public boolean normal = false;
	
	public RecipeManager() {	
	}
	
	public RecipeManager(NBTTagCompound nbtTagCompound) {	
		this.readFromNBT(nbtTagCompound);
	}
	
	public RecipeManager(World world) {
		this.world = world;
	}
	
	private static boolean inList(ItemStack itemstack) { 
		for(String listItem : ProxyCommon.config.item_restrictions.list) {
			FMLLog.info(listItem);
			if(listItem.startsWith("/") && listItem.endsWith("/")) {
				return itemstack.getItem().getRegistryName().toString().matches(listItem.substring(1,listItem.length()-1));
			}else if(listItem.contains(":")) {
				if(listItem.contains("/")) { 
					if(listItem.split(":")[0].equals(itemstack.getItem().getRegistryName().getResourceDomain())&&(listItem.split("/")[0].split(":")[1].equals(itemstack.getUnlocalizedName())||listItem.split("/")[0].split(":")[1].equals(itemstack.getItem().getRegistryName().getResourcePath()))&&listItem.split("/")[1].equals(String.valueOf(itemstack.getMetadata()))){ // namespace:unlocalized_name/metadata
						return true;
					}
				}else if(listItem.split(":")[0].equals(itemstack.getItem().getRegistryName().getResourceDomain())&&(listItem.split(":")[1].equals(itemstack.getUnlocalizedName())||listItem.split(":")[1].equals(itemstack.getItem().getRegistryName().getResourcePath()))){ // namespace:unlocalized_name
					return true;
				}
			}else {
				if(listItem.contains("/")){
					if((listItem.split("/")[0].equals(itemstack.getUnlocalizedName())||listItem.split("/")[0].equals(itemstack.getItem().getRegistryName().getResourcePath()))&&listItem.split("/")[1].equals(String.valueOf(itemstack.getMetadata()))){ // unlocalized_name/metadata
						return true;
					}
				}else if(listItem.equals(itemstack.getItem().getRegistryName().getResourceDomain())|| // namespace
					listItem.equals(itemstack.getUnlocalizedName())||listItem.equals(itemstack.getItem().getRegistryName().getResourcePath())) {// unlocalized_name
					return true;
				}
			}
		}
		return false;
	}
	
	public ItemStack getOutput(EditableRecipe recipe) {
		if(this.normal || this.unlockedItems.contains(RecipeUnlock.getItemString(recipe.getRealOutput())) 
				|| this.recipes.getOutput(recipe) == null || this.unlockedItems.contains(RecipeUnlock.getItemString(this.recipes.getOutput(recipe)))) {
			return null;
		}else {
			return this.recipes.getOutput(recipe);
		}
	}
	
	public void unlockItem(ItemStack stack) {
		String stack_string = RecipeUnlock.getItemString(stack);
		if(stack_string.equals("")) {
			return;
		}
		this.unlockedItems.add(stack_string);
		this.save();
	}
	
	public boolean isUnlocked(ItemStack stack) {
		return stack == null || this.unlockedItems.contains(RecipeUnlock.getItemString(stack));
	}
	
	public void storeRecipes() {
		this.normalRecipes = new RecipeRegistry();
		
		for(IRecipe recipe : CraftingManager.getInstance().getRecipeList()) {
			if(recipe instanceof EditableRecipe&&(((!ProxyCommon.config.item_restrictions.is_white_list)&&(!inList(((EditableRecipe) recipe).getRealOutput())))||
					(ProxyCommon.config.item_restrictions.is_white_list&&inList(((EditableRecipe) recipe).getRealOutput())))) {
				this.normalRecipes.add(new RecipeSave((EditableRecipe)recipe));
			}
		}
		
		Map<ItemStack, ItemStack> smeltingList = FurnaceRecipes.instance().getSmeltingList();
		for(ItemStack input : smeltingList.keySet()) {
			if(((!ProxyCommon.config.item_restrictions.is_white_list)&&(!inList(input)))||
					(ProxyCommon.config.item_restrictions.is_white_list&&inList(input))) {
				normalRecipes.add(new RecipeSave(input, smeltingList.get(input), lastID++));
			}
		}
	}
	
	public boolean load() {
		if(world != null) {
			File filename = Paths.get(world.getSaveHandler().getWorldDirectory().getPath(), folder, file).toFile();
			if(this.normalRecipes == null) {
				this.storeRecipes();
			}
			if(filename.exists()) {
				if(this.normalRecipes == null) {
					this.storeRecipes();
				}
				try {
	                FileInputStream fileinputstream = new FileInputStream(filename);
	                NBTTagCompound nbttagcompound = CompressedStreamTools.readCompressed(fileinputstream);
	                fileinputstream.close();
	                this.readFromNBT(nbttagcompound.getCompoundTag("data"));
				} catch (IOException e) {
					e.printStackTrace();
				}
				if(this.normal) {
					this.normalRecipes.applyFurnace();
				}else {
					this.recipes.applyFurnace();
				}
				ModMain.network.sendToAll(new MessageRecipes(this));
				return true;
			}else {
				this.unlockedItems.clear();
				this.randomise();
				this.save();
				return false;
			}
		}
		return false;
	}

	public void randomise() {
		this.normal = false;
		this.recipes = new RecipeRegistry();
		Random random = new Random();
		List<ItemStack> outputs = this.normalRecipes.getOutputs();
		for(RecipeSave recipe : this.normalRecipes.getRecipes()) {
			if(recipe.getRealOutput() != null&&!this.unlockedItems.contains(RecipeUnlock.getItemString(recipe.getRealOutput()))) {
				ItemStack newOutput = outputs.remove(random.nextInt(outputs.size()));
				while(this.unlockedItems.contains(RecipeUnlock.getItemString(newOutput))) {
					newOutput = outputs.remove(random.nextInt(outputs.size()));
				}
				recipes.add(recipe.copy().setOutput(newOutput));
			}
		}
		this.recipes.applyFurnace();
		this.save();
	}
	
	public void readFromNBT(NBTTagCompound nbt) {
		this.normal = nbt.getBoolean("normal");
		this.recipes = new RecipeRegistry((NBTTagCompound)nbt.getTag("recipes"));
		this.unlockedItems.clear();
		if(nbt.hasKey("unlocked")) {
			NBTTagList unlocked_nbt = nbt.getTagList("unlocked", 8);
			for(int i=0; i < unlocked_nbt.tagCount(); i++) {
				this.unlockedItems.add(unlocked_nbt.getStringTagAt(i));
			}
		}
	}
	
	public void save(){
		if(world != null) {
			File filename = Paths.get(world.getSaveHandler().getWorldDirectory().getPath(), folder, file).toFile();
			
			try {
				if(!filename.exists()) {
					filename.createNewFile();
				}
				NBTTagCompound nbttagcompound = new NBTTagCompound();
	            nbttagcompound.setTag("data", this.writeToNBT(new NBTTagCompound()));
	            FileOutputStream fileoutputstream = new FileOutputStream(filename);
	            CompressedStreamTools.writeCompressed(nbttagcompound, fileoutputstream);
	            fileoutputstream.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	public void setNormal() {
		this.normal = true;
		this.normalRecipes.applyFurnace();
		this.save();
	}

	public static void wrapAll() {
		List<String> classes = new ArrayList<String>();
		
		List<IRecipe> recipesToWrap = new LinkedList<>();
		Iterator<IRecipe> it = CraftingManager.getInstance().getRecipeList().iterator();
		while(it.hasNext()) {
			IRecipe recipe = it.next();
			if(!classes.contains(recipe.getClass().getCanonicalName())) {
				classes.add(recipe.getClass().getCanonicalName());
			}
			for(String cfgStr : ProxyCommon.config.class_restrictions.classes) {
				if(recipe.getClass().getCanonicalName().equals(cfgStr)) {
					recipesToWrap.add(recipe);
					break;
				}
			}
		}
		Collections.sort(classes);
		try {
			ProxyCommon.config.setClasses(classes.toArray(new String[0]), ProxyCommon.configFile);
		} catch (IOException e) {
			ModTools.logError("Error saving dectected classes to config file");
			e.printStackTrace();
		}
        for (IRecipe recipe : recipesToWrap) {
        	
        	CraftingManager.getInstance().getRecipeList().remove(recipe);
            EditableRecipe newRecipe = new EditableRecipe(recipe, lastID++);
            CraftingManager.getInstance().addRecipe(newRecipe);
        }
	}
	
	public NBTTagCompound writeToNBT(NBTTagCompound compound) {
		compound.setBoolean("normal", this.normal);
		compound.setTag("recipes", this.recipes.writeToNBT(new NBTTagCompound()));
		NBTTagList unlocked_nbt = new NBTTagList();
		for(String item : this.unlockedItems) {
			unlocked_nbt.appendTag(new NBTTagString(item));
		}
		compound.setTag("unlocked", unlocked_nbt);
		return compound;
	}

	public boolean canUnlock(ItemStack stack) {
		if((!this.unlockedItems.contains(RecipeUnlock.getItemString(stack)))){
			for(ItemStack rStack : this.normalRecipes.getOutputs()) {
				if(RecipeUnlock.getItemString(stack).equals(RecipeUnlock.getItemString(rStack))) {
					return true;
				}
			}
		}
		return false;
	}

	public ArrayList<String> getUnlockedItems() {
		return new ArrayList<String>(this.unlockedItems);
	}
	
	public void addUnlockedItem(String str) {
		this.unlockedItems.add(str);
	}
}
