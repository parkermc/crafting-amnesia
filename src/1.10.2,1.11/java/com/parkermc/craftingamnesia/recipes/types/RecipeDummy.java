package com.parkermc.craftingamnesia.recipes.types;

import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.IRecipe;
import net.minecraft.nbt.NBTTagCompound;

public class RecipeDummy<T extends IRecipe> extends RecipeType<T> {
	
	public RecipeDummy(T recipe) {
		this.type = Type.Dummy;
		this.recipe = recipe;
	}
	
	@Override
	public ItemStack getOutput() {
		return null;
	}

	@Override
	public void readFromNBT(NBTTagCompound nbt) {
	}
	
	@Override
	public RecipeDummy<T> setOutput(ItemStack item) {
		return this;
	}

	@Override
	public NBTTagCompound writeToNBT(NBTTagCompound compound) {
		return null;
	}

	@Override
	public RecipeDummy<T> copy() {
		return new RecipeDummy<T>(this.recipe);
	}
}
