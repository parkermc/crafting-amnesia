package com.parkermc.craftingamnesia.recipes.types;

import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.CraftingManager;
import net.minecraft.item.crafting.IRecipe;
import net.minecraft.nbt.NBTTagCompound;


public abstract class RecipeType<T extends IRecipe> {
	public enum Type{
		Shaped(0),
		Shapeless(1),
		OreShaped(2),
		OreShapeless(3),
		Dummy(4),
		Furnace(5);
		
		final int value;
		private Type(int value) {
			this.value = value;
		}
	};
	
	protected T recipe;
	public Type type;
	
	public RecipeType() {	
	}
	
	public static RecipeType<? extends IRecipe> New(NBTTagCompound nbt) {
		int type = nbt.getByte("type");
		if(type == Type.Shaped.value) {
			return new RecipeShaped(nbt);
		}else if(type == Type.Shapeless.value) {
			return new RecipeShapeless(nbt);
		}else if(type == Type.OreShapeless.value) {
			return new RecipeOreShapeless(nbt);
		}else if(type == Type.OreShaped.value) {
			return new RecipeOreShaped(nbt);
		}else {
			return new RecipeFurnace(nbt);
		}
	}
	
	public void apply() {
		CraftingManager.getInstance().addRecipe(recipe);
	}
	
	public abstract ItemStack getOutput();
	public abstract void readFromNBT(NBTTagCompound nbt);
	public abstract RecipeType<T> setOutput(ItemStack item);
	public abstract NBTTagCompound writeToNBT(NBTTagCompound compound);
	public abstract RecipeType<T> copy();
}
