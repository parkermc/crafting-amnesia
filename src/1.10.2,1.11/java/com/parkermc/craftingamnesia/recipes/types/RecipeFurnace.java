package com.parkermc.craftingamnesia.recipes.types;

import org.apache.logging.log4j.Level;

import com.parkermc.amnesia.ModTools;

import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.FurnaceRecipes;
import net.minecraft.item.crafting.IRecipe;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraftforge.fml.common.FMLLog;

public class RecipeFurnace extends RecipeType<IRecipe> {
	private ItemStack input;
	private ItemStack output;
	
	public RecipeFurnace(ItemStack input, ItemStack output) {
		this.type = Type.Furnace;
		this.input = input;
		this.output = output;
	}
	
	public RecipeFurnace(NBTTagCompound nbt) {
		this.type = Type.Furnace;
		this.readFromNBT(nbt);
	}

	private RecipeFurnace() {
	}
		
	@Override
	public RecipeFurnace copy() {
		RecipeFurnace obj = new RecipeFurnace();
		obj.type = this.type;
		obj.input = this.input.copy();
		obj.output = this.output.copy();
		return obj;
	}

	@Override
	public void apply() {
		for(ItemStack item : FurnaceRecipes.instance().getSmeltingList().keySet()) {
			if(item.getItem().getRegistryName().equals(this.input.getItem().getRegistryName())&&item.getMetadata() == this.input.getMetadata()) {
				FurnaceRecipes.instance().getSmeltingList().put(item, this.output);
				return;
			}
		}
		FMLLog.log(Level.ERROR, "Error setting furnace recipe");
	}
	
	@Override
	public void readFromNBT(NBTTagCompound nbt) {
		this.input = ModTools.getItemStack(nbt.getCompoundTag("input"));
		this.output = ModTools.getItemStack(nbt.getCompoundTag("output"));
	}
	
	@Override
	public NBTTagCompound writeToNBT(NBTTagCompound compound) {
		compound.setByte("type", (byte)this.type.value);
		compound.setTag("input", this.input.writeToNBT(new NBTTagCompound()));
		compound.setTag("output", this.output.writeToNBT(new NBTTagCompound()));
		return compound;
	}

	@Override
	public ItemStack getOutput() {
		return this.output;
	}

	@Override
	public RecipeType<IRecipe> setOutput(ItemStack item) {
		this.output = item;
		return this;
	}
}
