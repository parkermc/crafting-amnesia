package com.parkermc.craftingamnesia.recipes.types;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.Level;

import com.parkermc.amnesia.ModTools;
import com.parkermc.craftingamnesia.recipes.oreitem.RecipeOreType;

import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraftforge.fml.common.FMLLog;
import net.minecraftforge.oredict.ShapelessOreRecipe;

public class RecipeOreShapeless extends RecipeType<ShapelessOreRecipe> {
	List<RecipeOreType<?>> items = new ArrayList<RecipeOreType<?>>(); 
	
	public RecipeOreShapeless(ShapelessOreRecipe recipe) {
		this.type = Type.OreShapeless;
		this.recipe = recipe;
		for(Object o : recipe.getInput()) {
			if(o instanceof ItemStack) {
				this.items.add(RecipeOreType.New((ItemStack) o)); 
			}else if (o instanceof List<?>&&((List<?>)o).size() > 0) {
				List<ItemStack> convertedList = new ArrayList<ItemStack>();
				for(Object o2 : ((List<?>)o)) {
					if(o2 instanceof ItemStack) {
						convertedList.add((ItemStack)o2);
					}else {
						FMLLog.log(Level.ERROR, "Don't know what to do with this type in ore shapeless: %s", o2.getClass().getName());
					}	
				}
				this.items.add(RecipeOreType.New(convertedList));
			}else {
				FMLLog.log(Level.ERROR, "Don't know what to do with this type in ore shapeless: %s", o.getClass().getName());
			} 
		}
	}
	
	public RecipeOreShapeless(NBTTagCompound nbt) {
		this.type = Type.OreShapeless;
		this.readFromNBT(nbt);
	}

	private RecipeOreShapeless() {
	}

	@Override
	public ItemStack getOutput() {
		return recipe.getRecipeOutput();
	}
	
	@Override
	public void readFromNBT(NBTTagCompound nbt) {
		// Assume type is right
		NBTTagList inputTag = nbt.getTagList("input", 10);
		for(int i=0;i<inputTag.tagCount();i++) {
			this.items.add(RecipeOreType.New(inputTag.getCompoundTagAt(i)));
		}
		this.setOutput(ModTools.getItemStack(nbt.getCompoundTag("output")));
	}
	
	@Override
	public RecipeOreShapeless setOutput(ItemStack outItem) {
		List<Object> input = new ArrayList<Object>();
		for(RecipeOreType<?> item : this.items) {
			input.add(item.get());
		}
		this.recipe = new ShapelessOreRecipe(outItem, input.toArray());
		return this;
	}
	
	@Override
	public NBTTagCompound writeToNBT(NBTTagCompound compound) {
		compound.setByte("type", (byte) type.value); // Set the type
		
		NBTTagList inputTag = new NBTTagList(); // Create the input items tag
		for(RecipeOreType<?> item : this.items) {
			inputTag.appendTag(item.writeToNBT(new NBTTagCompound()));
		}
		// Set the input and output tags
		compound.setTag("input", inputTag);
		compound.setTag("output", this.recipe.getRecipeOutput().writeToNBT(new NBTTagCompound()));
		return compound;
	}
	
	@Override
	public RecipeOreShapeless copy() {
		RecipeOreShapeless obj = new RecipeOreShapeless();
		obj.recipe = this.recipe;
		obj.type = this.type;
		obj.items = this.items;
		return obj;
	}
}
