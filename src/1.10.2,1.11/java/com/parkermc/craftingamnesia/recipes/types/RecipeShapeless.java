package com.parkermc.craftingamnesia.recipes.types;

import java.util.ArrayList;
import java.util.List;

import com.parkermc.amnesia.ModTools;

import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.ShapelessRecipes;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;

public class RecipeShapeless extends RecipeType<ShapelessRecipes> {
	
	public RecipeShapeless(ShapelessRecipes recipe) {
		this.type = Type.Shapeless;
		this.recipe = recipe;
	}
	
	public RecipeShapeless(NBTTagCompound nbt) {
		this.type = Type.Shapeless;
		this.readFromNBT(nbt);
	}

	private RecipeShapeless() {
	}

	@Override
	public ItemStack getOutput() {
		return recipe.getRecipeOutput();
	}
	
	@Override
	public void readFromNBT(NBTTagCompound nbt) {
		// Assume type is right
		List<ItemStack> input = new ArrayList<ItemStack>();
		NBTTagList inputTag = nbt.getTagList("input", 10);
		for(int i=0;i<inputTag.tagCount();i++) {
			if(((NBTTagCompound)inputTag.get(i)).getSize() == 0) {
				input.add(null);
			}else {
				input.add(ModTools.getItemStack((NBTTagCompound)inputTag.get(i)));
			}
		}
		this.recipe = new ShapelessRecipes(ModTools.getItemStack(nbt.getCompoundTag("output")), input);
	}
	
	@Override
	public RecipeShapeless setOutput(ItemStack item) {
		this.recipe = new ShapelessRecipes(item, this.recipe.recipeItems);
		return this;
	}

	@Override
	public NBTTagCompound writeToNBT(NBTTagCompound compound) {
		compound.setByte("type", (byte) type.value); // Set the type
		
		NBTTagList inputTag = new NBTTagList(); // Create the input items tag
		for (ItemStack item : this.recipe.recipeItems) { // Add the items
			if(item == null) {
				inputTag.appendTag(new NBTTagCompound());
			}else {
				inputTag.appendTag(item.writeToNBT(new NBTTagCompound()));
			}
		}
		// Set the input and output tags
		compound.setTag("input", inputTag);
		compound.setTag("output", this.recipe.getRecipeOutput().writeToNBT(new NBTTagCompound()));
		return compound;
	}
	
	@Override
	public RecipeShapeless copy() {
		RecipeShapeless obj = new RecipeShapeless();
		obj.recipe = this.recipe;
		obj.type = this.type;
		return obj;
	}
}
