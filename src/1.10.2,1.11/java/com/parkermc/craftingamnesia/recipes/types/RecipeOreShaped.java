package com.parkermc.craftingamnesia.recipes.types;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.Level;

import com.parkermc.amnesia.ModTools;
import com.parkermc.craftingamnesia.recipes.oreitem.RecipeOreNone;
import com.parkermc.craftingamnesia.recipes.oreitem.RecipeOreType;

import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraftforge.fml.common.FMLLog;
import net.minecraftforge.oredict.ShapedOreRecipe;

public class RecipeOreShaped extends RecipeType<ShapedOreRecipe> {
	
	private List<RecipeOreType<?>> items = new ArrayList<RecipeOreType<?>>();
	private boolean mirrored;
	private int width;
	private int height; 
	
	public RecipeOreShaped(ShapedOreRecipe recipe) {
		this.type = Type.OreShaped;
		this.recipe = recipe;
		try {
			Field field = ShapedOreRecipe.class.getDeclaredField("mirrored");
			field.setAccessible(true);
			this.mirrored = (boolean)field.get(recipe);
		} catch (IllegalArgumentException | IllegalAccessException | NoSuchFieldException | SecurityException e) {
			FMLLog.log(Level.ERROR, "Couldn't get mirrored field.");
			e.printStackTrace();
			return;
		}
		try {
			Field field = ShapedOreRecipe.class.getDeclaredField("width");
			field.setAccessible(true);
			this.width = (int)field.get(recipe);
		} catch (IllegalArgumentException | IllegalAccessException | NoSuchFieldException | SecurityException e) {
			FMLLog.log(Level.ERROR, "Couldn't get width field.");
			e.printStackTrace();
			return;
		}
		try {
			Field field = ShapedOreRecipe.class.getDeclaredField("height");
			field.setAccessible(true);
			this.height = (int)field.get(recipe);
		} catch (IllegalArgumentException | IllegalAccessException | NoSuchFieldException | SecurityException e) {
			FMLLog.log(Level.ERROR, "Couldn't get height field.");
			e.printStackTrace();
			return;
		}
		for(Object o : recipe.getInput()) {
			if(o instanceof ItemStack) {
				this.items.add(RecipeOreType.New((ItemStack) o)); 
			}else if (o instanceof List<?>&&((List<?>)o).size() > 0) {
				List<ItemStack> convertedList = new ArrayList<ItemStack>();
				for(Object o2 : ((List<?>)o)) {
					if(o2 instanceof ItemStack) {
						convertedList.add((ItemStack)o2);
					}else {
						FMLLog.log(Level.ERROR, "Don't know what to do with this type in ore shaped: %s", o2.getClass().getName());
					}	
				}
				this.items.add(RecipeOreType.New(convertedList));
			}else if(o==null){
				this.items.add(new RecipeOreNone());
			}else {
				FMLLog.log(Level.ERROR, "Don't know what to do with this type in ore shaped: %s", o.getClass().getName());
			} 
		}
	}
	
	public RecipeOreShaped(NBTTagCompound nbt) {
		this.type = Type.OreShaped;
		this.readFromNBT(nbt);
	}

	private RecipeOreShaped() {
	}

	@Override
	public ItemStack getOutput() {
		return recipe.getRecipeOutput();
	}
	
	@Override
	public void readFromNBT(NBTTagCompound nbt) {
		// Assume type is right
		this.mirrored = nbt.getBoolean("mirrored");
		this.width = nbt.getInteger("width");
		this.height = nbt.getInteger("height");
		NBTTagList inputTag = nbt.getTagList("input", 10);
		for(int i=0;i<inputTag.tagCount();i++) {
			this.items.add(RecipeOreType.New(inputTag.getCompoundTagAt(i)));
		}
		this.setOutput(ModTools.getItemStack(nbt.getCompoundTag("output")));
	}
	
	@Override
	public RecipeOreShaped setOutput(ItemStack outItem) {
		Map<Object, Character> charList = new HashMap<Object, Character>();
		List<Object> input = new ArrayList<Object>();
		input.add(this.mirrored);
		input.add("");
		char key = 'A';
		int row = 1;
		int col = 0;
		for(RecipeOreType<?> item : this.items) {
			if(!(item instanceof RecipeOreNone)) {
				if(!charList.containsKey(item.get())) {
					charList.put(item.get(), key);
					key++;
				}
				input.set(row, input.get(row)+String.valueOf(charList.get(item.get())));
			}else {
				input.set(row, input.get(row)+" ");
			}
			col++;
			if(col==width) {
				col = 0;
				row++;
				if(row <= height) {
					input.add("");
				}
			}
		}
		for(Object item : charList.keySet()) {
			input.add(charList.get(item));
			input.add(item);
		}
		this.recipe = new ShapedOreRecipe(outItem, input.toArray());
		return this;
	}
	
	@Override
	public NBTTagCompound writeToNBT(NBTTagCompound compound) {
		compound.setByte("type", (byte) type.value); // Set the type
		compound.setBoolean("mirrored", this.mirrored);
		compound.setInteger("width", this.width);
		compound.setInteger("height", this.height);
		NBTTagList inputTag = new NBTTagList(); // Create the input items tag
		for(RecipeOreType<?> item : this.items) {
			inputTag.appendTag(item.writeToNBT(new NBTTagCompound()));
		}
		// Set the input and output tags
		compound.setTag("input", inputTag);
		compound.setTag("output", this.recipe.getRecipeOutput().writeToNBT(new NBTTagCompound()));
		return compound;
	}

	@Override
	public RecipeOreShaped copy() {
		RecipeOreShaped obj = new RecipeOreShaped();
		obj.recipe = this.recipe;
		obj.type = this.type;
		obj.items = this.items;
		obj.mirrored = this.mirrored;
		obj.width = this.width;
		obj.height = this.height; 
		return obj;
	}
}
