package com.parkermc.craftingamnesia.recipes.types;

import java.util.ArrayList;
import java.util.List;

import com.parkermc.amnesia.ModTools;

import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.ShapedRecipes;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;

public class RecipeShaped extends RecipeType<ShapedRecipes> {
	
	public RecipeShaped(ShapedRecipes recipe) {
		this.type = Type.Shaped;
		this.recipe = recipe;
	}
	
	public RecipeShaped(NBTTagCompound nbt) {
		this.type = Type.Shaped;
		this.readFromNBT(nbt);
	}
	
	private RecipeShaped() {
	}

	@Override
	public ItemStack getOutput() {
		return recipe.getRecipeOutput();
	}

	@Override
	public void readFromNBT(NBTTagCompound nbt) {
		// Assume type is right
		List<ItemStack> input = new ArrayList<ItemStack>();
		NBTTagList inputTag = nbt.getTagList("input", 10);
		for(int i=0;i<inputTag.tagCount();i++) {
			if(((NBTTagCompound)inputTag.get(i)).getSize() == 0) {
				input.add(null);
			}else {
				input.add(ModTools.getItemStack(((NBTTagCompound)inputTag.get(i))));
			}
		}
		this.recipe = new ShapedRecipes(nbt.getInteger("width"), nbt.getInteger("height"), input.toArray(new ItemStack[input.size()]), ModTools.getItemStack(nbt.getCompoundTag("output")));
	}
	
	@Override
	public RecipeShaped setOutput(ItemStack item) {
		this.recipe = new ShapedRecipes(this.recipe.recipeWidth, this.recipe.recipeHeight, this.recipe.recipeItems, item);
		return this;
	}

	@Override
	public NBTTagCompound writeToNBT(NBTTagCompound compound) {
		compound.setByte("type", (byte) type.value); // Set the type
		compound.setInteger("width", this.recipe.recipeWidth); // Set the size
		compound.setInteger("height", this.recipe.recipeHeight);
		
		NBTTagList inputTag = new NBTTagList(); // Create the input items tag
		for (ItemStack item : this.recipe.recipeItems) { // Add the items
			if(item == null) {
				inputTag.appendTag(new NBTTagCompound());
			}else {
				inputTag.appendTag(item.writeToNBT(new NBTTagCompound()));
			}
		}
		// Set the input and output tags
		compound.setTag("input", inputTag);
		compound.setTag("output", this.recipe.getRecipeOutput().writeToNBT(new NBTTagCompound()));
		return compound;
	}
	
	@Override
	public RecipeShaped copy() {
		RecipeShaped obj = new RecipeShaped();
		obj.recipe = this.recipe;
		obj.type = this.type;
		return obj;
	}
}
