package com.parkermc.craftingamnesia.recipes;

import java.util.ArrayList;
import java.util.List;

import com.parkermc.amnesia.ModTools;

import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.ShapedRecipes;
import net.minecraft.item.crafting.ShapelessRecipes;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraftforge.oredict.ShapedOreRecipe;
import net.minecraftforge.oredict.ShapelessOreRecipe;

public class RecipeSave {
	public enum Type{
		Crafting(0),
		Furnace(1);
		
		int value;
		Type(int value){
			this.value = value;
		}
	}
	public enum CraftingT{
		Shapeless(0),
		Shaped(1),
		ShapelessOre(2),
		ShapedOre(3),
		Other(4);
		
		int value;
		CraftingT(int value){
			this.value = value;
		}
	}
	private ItemStack input;
	public List<List<ItemStack>> iteml = new ArrayList<List<ItemStack>>();
	private ItemStack output;
	private ItemStack realOutput;
	
	public ItemStack getRealOutput() {
		return realOutput.copy();
	}

	public void setRealOutput(ItemStack realOutput) {
		this.realOutput = realOutput.copy();
	}

	private int recipeID;
	private Type type;
	private CraftingT craftingT;
	
	private RecipeSave() {
		
	}
	
	public RecipeSave(EditableRecipe recipe) {
		this.output = recipe.getRecipeOutput();
		this.recipeID = recipe.id;
		this.type = Type.Crafting;
		if(recipe.recipe instanceof ShapelessRecipes) {
			this.craftingT = CraftingT.Shapeless;
			for(ItemStack stack : ((ShapelessRecipes)recipe.recipe).recipeItems) {
				this.iteml.add(new ArrayList<ItemStack>());
				if(stack != null) {
					this.iteml.get(this.iteml.size()-1).add(stack);
				}
			}
		}else if(recipe.recipe instanceof ShapedRecipes) {
			this.craftingT = CraftingT.Shaped;
			for(ItemStack stack : ((ShapedRecipes)recipe.recipe).recipeItems) {
				this.iteml.add(new ArrayList<ItemStack>());
				if(stack != null) {
					this.iteml.get(this.iteml.size()-1).add(stack);
				}
			}
		}else if(recipe.recipe instanceof ShapelessOreRecipe) {
			this.craftingT = CraftingT.ShapelessOre;
			for(Object o : ((ShapelessOreRecipe)recipe.recipe).getInput()) {
				if(o == null) {
					this.iteml.add(new ArrayList<ItemStack>());
				}else if(o instanceof List) {
					this.iteml.add(new ArrayList<ItemStack>());
					for(Object o2 : (List<?>)o) {
						if(o2 == null) {
						}else if(o2 instanceof ItemStack) {
							this.iteml.get(this.iteml.size()-1).add((ItemStack)o2);
						}
					}
				}else if(o instanceof ItemStack) {
					this.iteml.add(new ArrayList<ItemStack>());
					this.iteml.get(this.iteml.size()-1).add((ItemStack)o);
				}
			}
		}else if(recipe.recipe instanceof ShapedOreRecipe) {
			this.craftingT = CraftingT.ShapedOre;
			for(Object o : ((ShapedOreRecipe)recipe.recipe).getInput()) {
				if(o == null) {
					this.iteml.add(new ArrayList<ItemStack>());
				}else if(o instanceof List) {
					this.iteml.add(new ArrayList<ItemStack>());
					for(Object o2 : (List<?>)o) {
						if(o2 == null) {
						}else if(o2 instanceof ItemStack) {
							this.iteml.get(this.iteml.size()-1).add((ItemStack)o2);
						}
					}
				}else if(o instanceof ItemStack) {
					this.iteml.add(new ArrayList<ItemStack>());
					this.iteml.get(this.iteml.size()-1).add((ItemStack)o);
				}
			}
		}else {
			this.craftingT = CraftingT.Other;
		}
	}
	
	public RecipeSave(ItemStack in, ItemStack out, int id) {
		this.input = in;
		this.output = out;
		this.recipeID = id;
		this.type = Type.Furnace;
		this.craftingT = CraftingT.Other;
	}
	
	public RecipeSave(NBTTagCompound nbt) {
		this.readFromNBT(nbt);
	}
	
	public RecipeSave copy() {
		RecipeSave out = new RecipeSave();
		if(this.input != null) {
			out.input = this.input;
		}
		out.output = this.output.copy();
		out.recipeID = this.recipeID;
		out.type = this.type;
		out.craftingT = this.craftingT;
		for(List<ItemStack> lstack : this.iteml) {
			out.iteml.add(new ArrayList<ItemStack>());
			for(ItemStack stack : lstack) {
				out.iteml.get(out.iteml.size()-1).add(stack.copy());
			}
		}
		return out;
	}
	
	@Override
	public boolean equals(Object obj) {
		if(obj instanceof RecipeSave) {
			if(this.type == ((RecipeSave)obj).type && this.craftingT == ((RecipeSave)obj).craftingT &&
					((this.input == null && ((RecipeSave)obj).input == null) || (this.input != null && this.input.equals(((RecipeSave)obj).input))) &&
					this.iteml.size() == ((RecipeSave)obj).iteml.size()) {
				for(int i=0;i<this.iteml.size();i++) {
					if(this.iteml.get(i).size() != ((RecipeSave)obj).iteml.get(i).size()) {
						return false;
					}
					for(int j=0;j<this.iteml.get(i).size();j++) {
						if(!(new ItemCompare(((RecipeSave)obj).iteml.get(i).get(j)).equals(new ItemCompare(this.iteml.get(i).get(j))))) {
							return false;
						}
					}
				}
				return true;
			}
		}
		return false;
	}
	
	public int getID() {
		return this.recipeID;
	}
	
	public ItemStack getInput() {
		return this.input;
	}
	
	public ItemStack getOutput() {
		return this.output;
	}
	
	public boolean isFurnace() {
		return this.type == Type.Furnace;
	}
	
	public void setID(int id) {
		this.recipeID = id;
	}
	
	public void readFromNBT(NBTTagCompound nbt) {
		this.output = ModTools.getItemStack(nbt.getCompoundTag("output"));
		if(nbt.hasKey("input")) {
			this.input = ModTools.getItemStack(nbt.getCompoundTag("input"));
		}
		int type = nbt.getByte("type");
		if(type == Type.Crafting.value) {
			this.type = Type.Crafting;
		}else {
			this.type = Type.Furnace;
		}
		type = nbt.getByte("ctype");
		if(type == CraftingT.Shapeless.value) {
			this.craftingT = CraftingT.Shapeless;
		}else if(type == CraftingT.Shaped.value) {
			this.craftingT = CraftingT.Shaped;
		}else if(type == CraftingT.ShapelessOre.value) {
			this.craftingT = CraftingT.ShapelessOre;
		}else if(type == CraftingT.ShapedOre.value) {
			this.craftingT = CraftingT.ShapedOre;
		}else if(type == CraftingT.Other.value) {
			this.craftingT = CraftingT.Other;
		}
		NBTTagList arr = nbt.getTagList("CItems", 9);
		for(int i=0;i<arr.tagCount();i++) {
			NBTTagList arr2 = (NBTTagList)arr.get(i);
			this.iteml.add(new ArrayList<ItemStack>());
			for(int j=0;j<arr2.tagCount();j++) {
				if(arr2.getCompoundTagAt(j).hasNoTags()) {
					this.iteml.get(this.iteml.size()-1).add(null);
				}else {
					this.iteml.get(this.iteml.size()-1).add(ModTools.getItemStack(arr2.getCompoundTagAt(j)));
				}
			}
		}
	}
	
	public RecipeSave setOutput(ItemStack out) {
		this.output = out;
		return this;
	}

	public NBTTagCompound writeToNBT(NBTTagCompound compound) {
		compound.setTag("output", this.output.writeToNBT(new NBTTagCompound()));
		if(this.input != null) {
			compound.setTag("input", this.input.writeToNBT(new NBTTagCompound()));
		}
		compound.setByte("type", (byte) this.type.value);
		compound.setByte("ctype", (byte) this.craftingT.value);
		NBTTagList arr = new NBTTagList();
		for(List<ItemStack> istack : this.iteml) {
			NBTTagList arr2 = new NBTTagList();
			for(ItemStack stack : istack) {
				if(stack == null) {
					arr2.appendTag(new NBTTagCompound());
				}else {
					arr2.appendTag(stack.writeToNBT(new NBTTagCompound()));
				}
			}
			arr.appendTag(arr2);
		}
		compound.setTag("CItems", arr);
		return compound;
	}
}
