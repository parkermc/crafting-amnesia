package com.parkermc.craftingamnesia;

import com.parkermc.craftingamnesia.items.*;

import net.minecraftforge.fml.common.registry.GameRegistry;

public class ModItems {
	public static ItemLock lock;
	
	public static void preInit() {
		GameRegistry.register(lock = new ItemLock());
	}
	
	public static void initModels() {
		lock.initModel();
	}
}
