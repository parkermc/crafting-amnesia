package com.parkermc.craftingamnesia;

import com.parkermc.craftingamnesia.recipes.RecipeUnlock;

import net.minecraft.item.ItemStack;
import net.minecraftforge.fml.common.registry.GameRegistry;

public class ModRecipes {
	
	public static void preInit() {
		GameRegistry.addShapedRecipe(new ItemStack(ModItems.lock, 1), 
				"RMr",
				'R', com.parkermc.amnesia.ModItems.reset,
				'M', com.parkermc.amnesia.ModBlocks.memories,
				'r', com.parkermc.amnesia.ModItems.cure);
		GameRegistry.addShapedRecipe(new ItemStack(ModItems.lock, 1), 
				"rMR",
				'R', com.parkermc.amnesia.ModItems.reset,
				'M', com.parkermc.amnesia.ModBlocks.memories,
				'r', com.parkermc.amnesia.ModItems.cure);
		
		GameRegistry.addRecipe(new RecipeUnlock());
	}
}
