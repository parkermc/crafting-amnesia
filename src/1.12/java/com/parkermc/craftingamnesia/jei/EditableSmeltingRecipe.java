package com.parkermc.craftingamnesia.jei;

import java.awt.Color;
import java.util.Collections;
import java.util.IllegalFormatException;
import java.util.List;

import mezz.jei.api.ingredients.IIngredients;
import mezz.jei.api.recipe.IRecipeWrapper;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.FurnaceRecipes;
import net.minecraft.util.text.translation.I18n;
import net.minecraftforge.fml.common.FMLLog;

@SuppressWarnings("deprecation")
public class EditableSmeltingRecipe implements IRecipeWrapper {
	private final List<List<ItemStack>> inputs;
	private final ItemStack output;

	public EditableSmeltingRecipe(List<ItemStack> inputs, ItemStack output) {
		this.inputs = Collections.singletonList(inputs);
		this.output = output;
	}

	@Override
	public void getIngredients(IIngredients ingredients) {
		ingredients.setInputLists(ItemStack.class, inputs);
		ingredients.setOutput(ItemStack.class, output);
	}

	@Override
	public void drawInfo(Minecraft minecraft, int recipeWidth, int recipeHeight, int mouseX, int mouseY) {
		FurnaceRecipes furnaceRecipes = FurnaceRecipes.instance();
		float experience = furnaceRecipes.getSmeltingExperience(output);
		if (experience > 0) {
			String experienceString = "";
			
			String s;
			if (I18n.canTranslate("gui.jei.category.smelting.experience")) {
				s = I18n.translateToLocal("gui.jei.category.smelting.experience");
			} else {
				s = I18n.translateToFallback("gui.jei.category.smelting.experience");
			}
			
			try {
				experienceString = String.format(s, experience);
			} catch (IllegalFormatException e) {
				FMLLog.log.error("Format error: {}", s, e);
				experienceString = "Format error: " + s;
			}
			FontRenderer fontRenderer = minecraft.fontRenderer;
			int stringWidth = fontRenderer.getStringWidth(experienceString);
			fontRenderer.drawString(experienceString, recipeWidth - stringWidth, 0, Color.gray.getRGB());
		}
	}
}
