package com.parkermc.craftingamnesia.jei;

import com.parkermc.craftingamnesia.recipes.EditableRecipe;

import mezz.jei.api.IJeiHelpers;
import mezz.jei.api.recipe.wrapper.IShapedCraftingRecipeWrapper;
import net.minecraft.item.crafting.ShapedRecipes;

public class EditableShapedRecipesWrapper extends EditableShapelessRecipeWrapper<EditableRecipe> implements IShapedCraftingRecipeWrapper {
	public EditableShapedRecipesWrapper(IJeiHelpers jeiHelpers, EditableRecipe recipe) {
		super(jeiHelpers, recipe);
	}

	@Override
	public int getWidth() {
		return ((ShapedRecipes)recipe.recipe).recipeWidth;
	}

	@Override
	public int getHeight() {
		return ((ShapedRecipes)recipe.recipe).recipeHeight;
	}
}
