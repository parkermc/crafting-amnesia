package com.parkermc.craftingamnesia.jei;

import com.parkermc.craftingamnesia.recipes.EditableRecipe;

import mezz.jei.api.IJeiHelpers;
import mezz.jei.api.recipe.IRecipeWrapper;
import net.minecraft.item.crafting.ShapedRecipes;
import net.minecraft.item.crafting.ShapelessRecipes;
import net.minecraftforge.oredict.ShapedOreRecipe;
import net.minecraftforge.oredict.ShapelessOreRecipe;

public class EditableRecipeWrapper {

	public static IRecipeWrapper New(IJeiHelpers jeiHelpers, EditableRecipe recipe) {
		if(recipe.recipe instanceof ShapedOreRecipe) {
			return new EditableShapedOreRecipeWrapper(jeiHelpers, recipe);
		}else if(recipe.recipe instanceof ShapedRecipes) {
			return new EditableShapedRecipesWrapper(jeiHelpers, recipe);
		}else if(recipe.recipe instanceof ShapelessOreRecipe || recipe.recipe instanceof ShapelessRecipes) {
			return new EditableShapelessRecipeWrapper<>(jeiHelpers, recipe);
		}
		return null;
	}

}
