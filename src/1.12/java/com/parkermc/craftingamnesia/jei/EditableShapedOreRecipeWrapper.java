package com.parkermc.craftingamnesia.jei;

import com.parkermc.craftingamnesia.recipes.EditableRecipe;

import mezz.jei.api.IJeiHelpers;
import mezz.jei.api.recipe.wrapper.IShapedCraftingRecipeWrapper;
import net.minecraftforge.oredict.ShapedOreRecipe;

public class EditableShapedOreRecipeWrapper extends EditableShapelessRecipeWrapper<EditableRecipe> implements IShapedCraftingRecipeWrapper {
	public EditableShapedOreRecipeWrapper(IJeiHelpers jeiHelpers, EditableRecipe recipe) {
		super(jeiHelpers, recipe);
	}

	@Override
	public int getWidth() {
		return ((ShapedOreRecipe)recipe.recipe).getWidth();
	}

	@Override
	public int getHeight() {
		return ((ShapedOreRecipe)recipe.recipe).getHeight();
	}

}
