package com.parkermc.craftingamnesia.events;

import com.parkermc.amnesia.events.AmnesiaEvent;
import com.parkermc.amnesia.events.IAmnesiaEvents;

import net.minecraft.client.util.RecipeBookClient;

@AmnesiaEvent
public class AmnesiaClientEvent implements IAmnesiaEvents{
	
	@Override
	public void normal() {
	}

	@Override
	public void random() {
	}

	@Override
	public void updatePost() {		
	}

	@Override
	public void updatePostClient() {
		RecipeBookClient.rebuildTable();
	}

	@Override
	public void cure() {
	}
}