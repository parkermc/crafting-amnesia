package com.parkermc.craftingamnesia.recipes;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.FurnaceRecipes;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.util.ResourceLocation;

public class RecipeRegistry {
	private Map<ResourceLocation,RecipeSave> recipes = new HashMap<ResourceLocation,RecipeSave>();
	
	public RecipeRegistry() {
	}
	
	public RecipeRegistry(NBTTagCompound nbt) {
		this.readFromNBT(nbt);
	}
	
	public void add(RecipeSave recipe) {
		this.recipes.put(recipe.getLocation(), recipe);
	}
	
	public void applyFurnace() {
		List<RecipeSave> furnaces = new ArrayList<RecipeSave>();
		for(RecipeSave recipe : this.recipes.values()) {
			if(recipe.isFurnace()) {
				furnaces.add(recipe);
			}
		}
		
		for(ItemStack input : FurnaceRecipes.instance().getSmeltingList().keySet()) {
			for(RecipeSave recipe : furnaces) {
				if(new ItemCompare(input).equals(new ItemCompare(recipe.getInput()))) {
					FurnaceRecipes.instance().getSmeltingList().replace(input, recipe.getOutput());
				}
			}
		}
	}
	
	public ItemStack getOutput(EditableRecipe recipe) {
		if(this.recipes.containsKey(recipe.getRegistryName())) {
			return this.recipes.get(recipe.getRegistryName()).getOutput().copy();
		}
		return null;
	}
	
	public List<ItemStack> getOutputs(){
		List<ItemStack> out = new ArrayList<ItemStack>();
		for(RecipeSave recipe : this.recipes.values()) {
			out.add(recipe.getOutput().copy());
		}
		return out;
	}
	
	public List<RecipeSave> getRecipes(){
		return new ArrayList<RecipeSave>(this.recipes.values());
	}
	
	public int size() {
		return this.recipes.size();
	}

	public void readFromNBT(NBTTagCompound nbt) {
		NBTTagList shapedTag = nbt.getTagList("recipes", 10);
		for(int i=0;i<shapedTag.tagCount();i++) {
			RecipeSave recipe = new RecipeSave(shapedTag.getCompoundTagAt(i));
			recipes.put(recipe.getLocation(), recipe);
		}
	}

	public NBTTagCompound writeToNBT(NBTTagCompound compound) {
		NBTTagList recipesTag = new NBTTagList();
		for(RecipeSave recipe : this.recipes.values()) {
			recipesTag.appendTag(recipe.writeToNBT(new NBTTagCompound()));
		}
		compound.setTag("recipes", recipesTag);
		return compound;
	}
}
