package com.parkermc.craftingamnesia.recipes;

import com.parkermc.amnesia.ModTools;

import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.IRecipe;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.nbt.NBTTagString;
import net.minecraft.util.ResourceLocation;

public class RecipeSave {
	public enum Type{
		Crafting(0),
		Furnace(1);
		
		int value;
		Type(int value){
			this.value = value;
		}
	}
	private ItemStack input;
	private ItemStack output;
	private ResourceLocation recipe;
	private Type type;
	
	private RecipeSave() {
		
	}
	
	public RecipeSave(IRecipe recipe) {
		this.output = recipe.getRecipeOutput();
		this.recipe = recipe.getRegistryName();
		this.type = Type.Crafting;
	}
	
	public RecipeSave(ItemStack in, ItemStack out) {
		this.input = in;
		this.output = out;
		this.recipe = new ResourceLocation("furnace_"+out.getUnlocalizedName()+":"+String.valueOf(out.getMetadata()));
		this.type = Type.Furnace;
	}
	
	public RecipeSave(NBTTagCompound nbt) {
		this.readFromNBT(nbt);
	}
	
	public RecipeSave copy() {
		RecipeSave out = new RecipeSave();
		if(this.input != null) {
			out.input = this.input;
		}
		out.output = this.output.copy();
		out.recipe = this.recipe;
		out.type = this.type;
		return out;
	}
	
	public ResourceLocation getLocation() {
		return this.recipe;
	}
	
	public ItemStack getInput() {
		return this.input;
	}
	
	public ItemStack getOutput() {
		return this.output;
	}
	
	public boolean isFurnace() {
		return this.type == Type.Furnace;
	}
	
	public void readFromNBT(NBTTagCompound nbt) {
		this.output = ModTools.getItemStack(nbt.getCompoundTag("output"));
		if(nbt.hasKey("input")) {
			this.input = ModTools.getItemStack(nbt.getCompoundTag("input"));
		}
		NBTTagList recipeTag = nbt.getTagList("recipe", 8);
		this.recipe = new ResourceLocation(recipeTag.getStringTagAt(0), recipeTag.getStringTagAt(1));
		int type = nbt.getByte("type");
		if(type == Type.Crafting.value) {
			this.type = Type.Crafting;
		}else {
			this.type = Type.Furnace;
		}
	}
	
	public RecipeSave setOutput(ItemStack out) {
		this.output = out;
		return this;
	}

	public NBTTagCompound writeToNBT(NBTTagCompound compound) {
		compound.setTag("output", this.output.writeToNBT(new NBTTagCompound()));
		if(this.input != null) {
			compound.setTag("input", this.input.writeToNBT(new NBTTagCompound()));
		}
		NBTTagList recipeTag = new NBTTagList();
		recipeTag.appendTag(new NBTTagString(this.recipe.getResourceDomain()));
		recipeTag.appendTag(new NBTTagString(this.recipe.getResourcePath()));
		compound.setTag("recipe", recipeTag);
		compound.setByte("type", (byte) this.type.value);
		return compound;
	}
}
