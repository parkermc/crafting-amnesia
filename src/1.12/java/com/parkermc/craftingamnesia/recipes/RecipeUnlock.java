package com.parkermc.craftingamnesia.recipes;

import com.parkermc.craftingamnesia.ModItems;
import com.parkermc.craftingamnesia.items.ItemLock;

import net.minecraft.inventory.InventoryCrafting;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.IRecipe;
import net.minecraft.util.NonNullList;
import net.minecraft.world.World;

public class RecipeUnlock extends net.minecraftforge.registries.IForgeRegistryEntry.Impl<IRecipe> implements IRecipe {
	
	@Override
	public boolean matches(InventoryCrafting inv, World worldIn) {
		
		boolean lock = false;
		boolean otherItem = false;
		ItemStack other = ItemStack.EMPTY;
		int size = inv.getWidth()*inv.getHeight();
		for(int i=0; i < size; i++) {
			if(!inv.getStackInSlot(i).isEmpty()) {
				if(ItemLock.isLock(inv.getStackInSlot(i).getItem())) {
					if(lock) {
						return false;
					}else {
						lock = true;
					}
				}else {
					if(otherItem) {
						return false;
					}else {
						otherItem = true;
						other = inv.getStackInSlot(i);
					}
				}
			}
		}
		if(worldIn != null && !worldIn.isRemote) {
			return lock && otherItem && RecipeManager.instance.canUnlock(other);
		}else {
			return lock && otherItem;
		}
	}

	@Override
	public ItemStack getCraftingResult(InventoryCrafting inv) {
		int size = inv.getWidth()*inv.getHeight();
		for(int i=0; i < size; i++) {
			if((!inv.getStackInSlot(i).isEmpty())&&!ItemLock.isLock(inv.getStackInSlot(i).getItem())) {
				ItemStack stack = inv.getStackInSlot(i).copy();
				stack.setCount(1);
				return stack;
			}
		}
		return ItemStack.EMPTY;
	}

	@Override
	public ItemStack getRecipeOutput() {
		return new ItemStack(ModItems.lock, 1);
	}

	@Override
    public NonNullList<ItemStack> getRemainingItems(InventoryCrafting inv){
		return NonNullList.withSize(inv.getSizeInventory(), ItemStack.EMPTY);
    }
	
	public static String getItemString(ItemStack stack) {
		if(stack != null && stack.getItem() != null) {
			return stack.getItem().getRegistryName().toString() + ":" + stack.getMetadata();
		}
		return "";		
	}

	@Override
	public boolean canFit(int width, int height) {
		return width*height >= 2;
	}
}
