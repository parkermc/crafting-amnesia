package com.parkermc.craftingamnesia.recipes;

import javax.annotation.Nullable;

import net.minecraft.inventory.InventoryCrafting;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.IRecipe;
import net.minecraft.item.crafting.Ingredient;
import net.minecraft.util.NonNullList;
import net.minecraft.util.ResourceLocation;
import net.minecraft.world.World;
import net.minecraftforge.registries.IForgeRegistryEntry;

public class EditableRecipe implements IForgeRegistryEntry<IRecipe>, IRecipe{
	public final IRecipe recipe;

	public EditableRecipe(IRecipe recipe) {
		this.recipe = recipe;
	}
	
	public ItemStack getRealOutput() {
		return recipe.getRecipeOutput();
	}
	
	@Override
	public ItemStack getCraftingResult(InventoryCrafting inv) {
		return this.getRecipeOutput();
	}
	
	@Override
	public ItemStack getRecipeOutput() {
		if(RecipeManager.instance != null) {
			ItemStack output = RecipeManager.instance.getOutput(this);
			if(output != null) {
				return output.copy();
			}
		}
		return this.recipe.getRecipeOutput().copy();
	}
	
	@Override
	public boolean matches(InventoryCrafting inv, World worldIn) {
		return recipe.matches(inv, worldIn);
	}


	@Override
	public boolean canFit(int width, int height) {
		return recipe.canFit(width, height);
	}
	
	@Override
	public NonNullList<ItemStack> getRemainingItems(InventoryCrafting inv){
        return recipe.getRemainingItems(inv);
    }

	@Override
	public NonNullList<Ingredient> getIngredients(){
        return recipe.getIngredients();
    }

	@Override
	public boolean isHidden(){
        return recipe.isHidden();
    }

	@Override
	public String getGroup(){
        return recipe.getGroup();
    }
	
    @Override
    public IRecipe setRegistryName(ResourceLocation name){
    	return recipe.setRegistryName(name); 
    }
    
    @Override
    @Nullable
    public final ResourceLocation getRegistryName(){
        return recipe.getRegistryName();
    }

    @Override
    public final Class<IRecipe> getRegistryType() {
    	return recipe.getRegistryType();
	}
}
