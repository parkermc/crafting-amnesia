package com.parkermc.craftingamnesia;

import com.parkermc.craftingamnesia.items.*;

import net.minecraftforge.fml.common.registry.ForgeRegistries;

public class ModItems {
	public static ItemLock lock;
	
	public static void preInit() {
		ForgeRegistries.ITEMS.register(lock = new ItemLock());
	}
	
	public static void initModels() {
		lock.initModel();
	}
}
