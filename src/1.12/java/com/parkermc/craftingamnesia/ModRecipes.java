package com.parkermc.craftingamnesia;

import com.parkermc.craftingamnesia.recipes.RecipeUnlock;

import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.common.registry.ForgeRegistries;
import net.minecraftforge.fml.common.registry.GameRegistry;

public class ModRecipes {
	
	public static void preInit() {
		GameRegistry.addShapedRecipe(new ResourceLocation(ModMain.MODID, "lock_1"), null, new ItemStack(ModItems.lock, 1), 
				"RMr",
				'R', com.parkermc.amnesia.ModItems.reset,
				'M', com.parkermc.amnesia.ModBlocks.memories,
				'r', com.parkermc.amnesia.ModItems.cure);
		GameRegistry.addShapedRecipe(new ResourceLocation(ModMain.MODID, "lock_2"), null, new ItemStack(ModItems.lock, 1), 
				"rMR",
				'R', com.parkermc.amnesia.ModItems.reset,
				'M', com.parkermc.amnesia.ModBlocks.memories,
				'r', com.parkermc.amnesia.ModItems.cure);
		
		ForgeRegistries.RECIPES.register((new RecipeUnlock()).setRegistryName(ModMain.MODID, "unlock"));
	}
}
