package com.parkermc.craftingamnesia.events;

import java.util.ArrayList;
import java.util.List;

import com.parkermc.amnesia.ModTools;
import com.parkermc.craftingamnesia.ModMain;
import com.parkermc.craftingamnesia.items.ItemLock;
import com.parkermc.craftingamnesia.network.MessageRecipes;
import com.parkermc.craftingamnesia.recipes.RecipeManager;

import net.minecraft.item.ItemStack;
import net.minecraftforge.event.world.WorldEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.PlayerEvent.ItemCraftedEvent;
import net.minecraftforge.fml.common.gameevent.PlayerEvent.PlayerLoggedInEvent;
import net.minecraftforge.fml.common.gameevent.TickEvent.WorldTickEvent;

public class EventHandlerWorld {
	List<ItemStack> toUnlock = new ArrayList<ItemStack>();
	
	@SubscribeEvent
	public void onWorldLoad(WorldEvent.Load event) {
		if(!event.getWorld().isRemote) {
			this.toUnlock.clear();
			RecipeManager.instance = new RecipeManager(event.getWorld());
			RecipeManager.instance.load();
		}
	}
	
	@SubscribeEvent
	public void onConnect(PlayerLoggedInEvent event) {
		ModMain.network.sendToAll(new MessageRecipes(RecipeManager.instance));
	}
	
	@SubscribeEvent
	public void onTick(WorldTickEvent event) {
		if(!event.world.isRemote) {
			boolean sendUpdate = this.toUnlock.size() > 0;
			while(this.toUnlock.size() > 0) {
				RecipeManager.instance.unlockItem(this.toUnlock.get(0));
				this.toUnlock.remove(0);
			}
			if(sendUpdate) {
				ModMain.network.sendToAll(new MessageRecipes(RecipeManager.instance));
			}
		}
	}
	
	@SubscribeEvent
	public void onItemCrafted(ItemCraftedEvent event) {
		if(!ModTools.getWorld(event.player).isRemote) {
			for(int i=0; i<event.craftMatrix.getSizeInventory(); i++) {
				if(event.craftMatrix.getStackInSlot(i) != null && event.craftMatrix.getStackInSlot(i).getItem() != null &&
						ItemLock.isLock(event.craftMatrix.getStackInSlot(i).getItem())) {
					for(int j=0; j<event.craftMatrix.getSizeInventory(); j++) {
						if(event.craftMatrix.getStackInSlot(j) != null && event.craftMatrix.getStackInSlot(j).getItem() != null &&
								!ItemLock.isLock(event.craftMatrix.getStackInSlot(j).getItem())&&RecipeManager.instance.canUnlock(event.craftMatrix.getStackInSlot(j))) {
							this.toUnlock.add(event.craftMatrix.getStackInSlot(j).copy());
						}
					}
				}
			}
		}
	}
}
