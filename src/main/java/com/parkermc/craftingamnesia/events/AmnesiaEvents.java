package com.parkermc.craftingamnesia.events;

import com.parkermc.amnesia.events.AmnesiaEvent;
import com.parkermc.amnesia.events.IAmnesiaEvents;
import com.parkermc.craftingamnesia.ModMain;
import com.parkermc.craftingamnesia.network.MessageRecipes;
import com.parkermc.craftingamnesia.recipes.RecipeManager;

@AmnesiaEvent
public class AmnesiaEvents implements IAmnesiaEvents{
	
	@Override
	public void normal() {
		RecipeManager.instance.setNormal();
		ModMain.network.sendToAll(new MessageRecipes(RecipeManager.instance));
	}

	@Override
	public void random() {
		RecipeManager.instance.randomise();
		ModMain.network.sendToAll(new MessageRecipes(RecipeManager.instance));
	}

	@Override
	public void updatePost() {		
	}

	@Override
	public void updatePostClient() {
	}

	@Override
	public void cure() {
	}
}