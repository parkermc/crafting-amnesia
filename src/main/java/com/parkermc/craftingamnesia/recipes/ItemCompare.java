package com.parkermc.craftingamnesia.recipes;

import net.minecraft.item.ItemStack;

public class ItemCompare {

	private String item;
	private int metadata;
	
	public ItemCompare(String str) {
		String[] split = str.split(":");
		this.metadata = Integer.parseInt(split[split.length-1]);
		this.item = split[0];
		for(int i=1;i<split.length-1;i++) {
			this.item += split[i];
		}
	}
	
	public ItemCompare(ItemStack stack) {
		if(stack != null && stack.getItem() != null) {
			this.item = stack.getItem().getRegistryName().toString();
			this.metadata = stack.getMetadata();
		}else {
			this.item = "";
		}
	}
	
	@Override
	public int hashCode() {
		return this.item.hashCode() * this.metadata;
	}
	
	@Override
	public boolean equals(Object obj) {
		if(obj.getClass() == ItemCompare.class) {
			if(this.item.equals(((ItemCompare)obj).item) && this.metadata == ((ItemCompare)obj).metadata) {
				return true;
			}
		}
		return false;
	}
	
	@Override
	public String toString() {
		return this.item + ":" + this.metadata;
	}
}
