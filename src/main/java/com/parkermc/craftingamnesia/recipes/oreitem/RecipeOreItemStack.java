package com.parkermc.craftingamnesia.recipes.oreitem;

import com.parkermc.amnesia.ModTools;

import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;

public class RecipeOreItemStack extends RecipeOreType<ItemStack>{
	
	private RecipeOreItemStack() {
		this.type = Type.ItemStack;
	}
	
	public RecipeOreItemStack(ItemStack item) {
		this();
		this.good = true;
		this.obj = item;
	}
	
	public RecipeOreItemStack(NBTTagCompound nbt) {
		this();
		this.good = true;
		this.readFromNBT(nbt);
	}
	
	@Override
	public void readFromNBT(NBTTagCompound nbt) {
		// Assume type is right
		this.obj = ModTools.getItemStack(nbt.getCompoundTag("item"));
	}

	@Override
	public NBTTagCompound writeToNBT(NBTTagCompound compound) {
		compound.setByte("type", (byte)this.type.value);
		compound.setTag("item", this.obj.writeToNBT(new NBTTagCompound()));
		return compound;
	}
}