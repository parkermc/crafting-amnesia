package com.parkermc.craftingamnesia.recipes.oreitem;

import java.util.List;

import org.apache.commons.lang3.ArrayUtils;

import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraftforge.oredict.OreDictionary;

public class RecipeOreString extends RecipeOreType<String>{
	
	private RecipeOreString() {
		this.type = Type.String;
	}
	
	public RecipeOreString(ItemStack item) {
		this();
		this.good = false;
		int[] oreIDs = OreDictionary.getOreIDs(item);
		if(oreIDs.length > 0) {
			for(int id : oreIDs) {
				String name = OreDictionary.getOreName(id); 
				if(OreDictionary.getOres(name).size() == 1) {
					this.obj = name;
					return;
				}
			}
		}
	}
	
	public RecipeOreString(List<ItemStack> list) {
		this();
		this.good = false;
		int[] oreIDs = OreDictionary.getOreIDs(list.get(0));
		for(int id : oreIDs) {
			boolean isGood = true;
			for(ItemStack item : list) {
				if(!ArrayUtils.contains(OreDictionary.getOreIDs(item), id)) {
					isGood = false;
					break;
				}
			}
			if(isGood) {
				this.obj = OreDictionary.getOreName(id);
				return;
			}
		}
		
	}
	
	public RecipeOreString(NBTTagCompound nbt) {
		this();
		this.good = true;
		this.readFromNBT(nbt);
	}
	
	@Override
	public void readFromNBT(NBTTagCompound nbt) {
		// Assume type is right
		this.obj = nbt.getString("str");
	}

	@Override
	public NBTTagCompound writeToNBT(NBTTagCompound compound) {
		compound.setByte("type", (byte)this.type.value);
		compound.setString("str", this.obj);
		return compound;
	}
}

