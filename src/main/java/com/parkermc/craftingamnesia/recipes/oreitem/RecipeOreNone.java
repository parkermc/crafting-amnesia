package com.parkermc.craftingamnesia.recipes.oreitem;

import net.minecraft.nbt.NBTTagCompound;

public class RecipeOreNone extends RecipeOreType<Object>{
	
	public RecipeOreNone() {
		this.type = Type.None;
		this.good = true;
	}
	
	public RecipeOreNone(NBTTagCompound nbt) {
		this();
		this.readFromNBT(nbt);
	}
	
	@Override
	public Object get() {
		return null;
	}
	
	@Override
	public void readFromNBT(NBTTagCompound nbt) {
	}

	@Override
	public NBTTagCompound writeToNBT(NBTTagCompound compound) {
		compound.setByte("type", (byte)this.type.value);
		return compound;
	}
}

