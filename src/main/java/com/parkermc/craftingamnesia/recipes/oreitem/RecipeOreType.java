package com.parkermc.craftingamnesia.recipes.oreitem;

import java.util.List;

import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;


public abstract class RecipeOreType<T extends Object> {
	public enum Type{
		String(0),
		ItemStack(1),
		None(2);
		
		final int value;
		private Type(int value) {
			this.value = value;
		}
	}
	
	protected boolean good;
	protected T obj;
	protected Type type;
	
	public RecipeOreType() {	
	}
	
	public static RecipeOreType<? extends Object> New(ItemStack item) {
		RecipeOreString oreString = new RecipeOreString(item);
		if(oreString.good) {
			return oreString;
		}
		return new RecipeOreItemStack(item);
	}
	
	public static RecipeOreType<? extends Object> New(List<ItemStack> list) {
		return new RecipeOreString(list);
	}
	
	public static RecipeOreType<? extends Object> New(NBTTagCompound nbt) {
		int type = nbt.getByte("type");
		if(type == Type.String.value) {
			return new RecipeOreString(nbt);
		}else if(type == Type.ItemStack.value){
			return new RecipeOreItemStack(nbt);
		}else {
			return new RecipeOreNone(nbt);
		}
	}
	
	public T get() {
		return obj;
	}
	
	public boolean getGood() {
		return this.good;
	}
	
	public abstract void readFromNBT(NBTTagCompound nbt);
	public abstract NBTTagCompound writeToNBT(NBTTagCompound compound);
	
	@Override
	public boolean equals(Object obj) {
		if(obj instanceof RecipeOreType<?>&&((RecipeOreType<?>) obj).type == this.type&&((RecipeOreType<?>) obj).get().equals(this.get())) {
			return true;
		}
		return false;
	}
}
