package com.parkermc.craftingamnesia;

import com.parkermc.craftingamnesia.network.MessageRecipes;
import com.parkermc.craftingamnesia.proxy.ProxyCommon;
import com.parkermc.craftingamnesia.recipes.RecipeManager;

import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventHandler;
import net.minecraftforge.fml.common.SidedProxy;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.common.network.NetworkRegistry;
import net.minecraftforge.fml.common.network.simpleimpl.SimpleNetworkWrapper;
import net.minecraftforge.fml.relauncher.Side;

@Mod(modid = ModMain.MODID, useMetadata = true)
public class ModMain{
    public static final String MODID = "craftingamnesia";
    
    @SidedProxy(clientSide = "com.parkermc.craftingamnesia.proxy.ProxyClient", serverSide = "com.parkermc.craftingamnesia.proxy.ProxyServer", modId = MODID)
    public static ProxyCommon proxy;
    
    public static SimpleNetworkWrapper network;
    
    @EventHandler
    public void preInit(FMLPreInitializationEvent event){
    	proxy.preInit(event);
    	network = NetworkRegistry.INSTANCE.newSimpleChannel(MODID);
    	network.registerMessage(MessageRecipes.Handler.class, MessageRecipes.class, 0, Side.CLIENT);
    }
    
    @EventHandler
    public void init(FMLInitializationEvent event) {
    	proxy.init(event);    
    }
    
    @EventHandler
    public void postInit(FMLPostInitializationEvent event) {
    	proxy.postInit(event);
    	RecipeManager.wrapAll();
    }

}

