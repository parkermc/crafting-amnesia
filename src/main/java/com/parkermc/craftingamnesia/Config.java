package com.parkermc.craftingamnesia;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class Config {
	public static final Gson GSON = new GsonBuilder().setPrettyPrinting().create();
	
	public ItemRestrictions item_restrictions = new ItemRestrictions();
	
	public class ItemRestrictions{
		public boolean is_white_list = false;
		public String[] list = new String[] {"amnesia:recipe_reset", "amnesia:recipe_cure", "amnesia:amnesia_clock", "amnesia:memories_compressed_1", "amnesia:memories_compressed_2", "amnesia:memories_compressed_3", "craftingamnesia:recipe_lock"};
	}
	
	public ClassRestrictions class_restrictions = new ClassRestrictions();
	
	public class ClassRestrictions{
		public String[] classes = new String[] {"net.minecraft.item.crafting.ShapedRecipes",
				"net.minecraft.item.crafting.ShapelessRecipes",
				"net.minecraftforge.oredict.ShapedOreRecipe",
				"net.minecraftforge.oredict.ShapelessOreRecipe"};

		public String[] detected_classes = new String[0];
	}
	
	public void setClasses(String[] list, File file) throws IOException {
		if(!Arrays.equals(this.class_restrictions.detected_classes, list)) {
			this.class_restrictions.detected_classes = list;
			this.Write(file);
		}
	}
	
	
	public static Config Read(File file) throws IOException {
		if(!file.exists()) {
			file.createNewFile();
			Config obj = new Config();
			obj.Write(file);
			return obj;
		}
		return GSON.fromJson(new FileReader(file), Config.class);
		
	}
	
	public void Write(File file) throws IOException {
		BufferedWriter writer = new BufferedWriter(new FileWriter(file));
		writer.write(GSON.toJson(this));
		writer.close();
	}
}
