package com.parkermc.craftingamnesia.items;

import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraft.item.Item;
import net.minecraftforge.client.model.ModelLoader;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class ItemLock extends Item{
	public static final String unlocalizedName = "recipe_lock";

	public ItemLock() {
		super();
		this.setCreativeTab(com.parkermc.amnesia.ModMain.tab);
		this.setUnlocalizedName(unlocalizedName);
		this.setRegistryName(unlocalizedName);
		this.setMaxStackSize(1);
	}
	
	@SideOnly(Side.CLIENT)
	public void initModel() {
		ModelLoader.setCustomModelResourceLocation(this, 0, new ModelResourceLocation(this.getRegistryName(), "inventory"));
	}
	
	public static boolean isLock(Item obj) {
		if (obj == null)
			return false;
		if (ItemLock.class != obj.getClass())
			return false;
		return true;
	}
}
