package com.parkermc.craftingamnesia.jei;

import com.parkermc.amnesia.events.AmnesiaEvent;
import com.parkermc.amnesia.events.IAmnesiaEvents;
import mezz.jei.api.IJeiRuntime;
import mezz.jei.api.IModPlugin;
import mezz.jei.api.IModRegistry;
import mezz.jei.api.ISubtypeRegistry;
import mezz.jei.api.JEIPlugin;
import mezz.jei.api.ingredients.IModIngredientRegistration;

@AmnesiaEvent
@JEIPlugin
public class JeiMain implements IModPlugin, IAmnesiaEvents{
	public static EditableRecipeRegistryPlugin recipeRegistryPlugin; 
	
	@Override
	public void onRuntimeAvailable(IJeiRuntime jeiRuntime) {
	}

	@Override
	public void registerItemSubtypes(ISubtypeRegistry subtypeRegistry) {
	}

	@Override
	public void registerIngredients(IModIngredientRegistration registry) {		
	}

	@Override
	public void register(IModRegistry registry) {
		recipeRegistryPlugin = new EditableRecipeRegistryPlugin(registry.getJeiHelpers());
		registry.addRecipeRegistryPlugin(recipeRegistryPlugin);
		
		// TODO remove then add smelting recipes
		/*
		registry.addRecipes(SmeltingRecipeMaker.getFurnaceRecipes(jeiHelpers), VanillaRecipeCategoryUid.SMELTING);
		*/

	}

	@Override
	public void random() {		
	}

	@Override
	public void normal() {		
	}

	@Override
	public void updatePost() {
	}

	@Override
	public void updatePostClient() {
		if(JeiMain.recipeRegistryPlugin != null) {
			JeiMain.recipeRegistryPlugin.Regen();
		}
	}

	@Override
	public void cure() {
	}

}
