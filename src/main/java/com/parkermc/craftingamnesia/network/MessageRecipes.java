package com.parkermc.craftingamnesia.network;

import com.parkermc.amnesia.events.AmnesiaEventHandler;
import com.parkermc.craftingamnesia.recipes.RecipeManager;
import com.parkermc.craftingamnesia.recipes.RecipeSave;

import io.netty.buffer.ByteBuf;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraftforge.fml.client.FMLClientHandler;
import net.minecraftforge.fml.common.network.ByteBufUtils;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class MessageRecipes implements IMessage {

	RecipeManager recipes;
	
	public MessageRecipes(){
	}
	
	public MessageRecipes(RecipeManager recipes){
		this.recipes = recipes;
	}
	
	@Override
	public void fromBytes(ByteBuf buf) {
		this.recipes = new RecipeManager();
		
		this.recipes.normal = buf.readBoolean();
		int len = buf.readInt();
		for(int i=0;i < len;i++) {
			this.recipes.recipes.add(new RecipeSave(ByteBufUtils.readTag(buf)));
		}
		len = buf.readInt();
		for(int i=0;i < len;i++) {
			this.recipes.addUnlockedItem(ByteBufUtils.readUTF8String(buf));
		}
	}

	@Override
	public void toBytes(ByteBuf buf) {
		buf.writeBoolean(this.recipes.normal);
		buf.writeInt(this.recipes.recipes.getRecipes().size());
		for(RecipeSave recipe : this.recipes.recipes.getRecipes()) {
			ByteBufUtils.writeTag(buf, recipe.writeToNBT(new NBTTagCompound()));
		}
		buf.writeInt(this.recipes.getUnlockedItems().size());
		for(String str : this.recipes.getUnlockedItems()) {
			ByteBufUtils.writeUTF8String(buf, str);
		}		
	}
	
	public static class Handler implements IMessageHandler<MessageRecipes, IMessage>{

		@SideOnly(Side.CLIENT)
		@Override
		public IMessage onMessage(MessageRecipes message, MessageContext ctx) {
			if(!FMLClientHandler.instance().getClient().isSingleplayer()) {
				RecipeManager.instance = message.recipes;
			}
			AmnesiaEventHandler.updatePostClient();
			return null;
		}
		
	}
}
